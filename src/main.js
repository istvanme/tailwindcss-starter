import "./components/main.css";

import Vue from 'vue';   // -> using webpack 'externals' to make Vue loaded from CDN via index.html available. See webpack.config

import { vendorScriptsInit } from "./js/_vendor.init";
import {siteScriptsInit} from "./js/_site.init";

vendorScriptsInit();
siteScriptsInit();


const { createApp, ref } = Vue;

const App = {
    setup() {
        const welcomeMessage = ref("Hello from Vue.js!");

        // Lorem
        const loremPara = ref("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, consequatur eius explicabo magni maiores, nam, neque nihil non nulla pariatur quas sint tempore ullam. Atque doloribus nisi porro similique voluptates.");
        const getLoremPara = (num = 1) => Array(num).fill(loremPara.value);

        return {
            welcomeMessage,

            getLoremPara
        };
    }
};

createApp(App).mount("#app");