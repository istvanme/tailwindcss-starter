const path = require('path');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    // Webpack base config
    entry: './src/main.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'dist.bundle.js'
    },
    externals: {
        vue: 'Vue'
    },
    devServer: {
        watchContentBase: true,
        contentBase: path.join(__dirname, 'dist'),
        writeToDisk: true,
        open: true
    },


    module: {
        rules: [

            // CSS
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: path.join(__dirname, 'dist'),
                            hmr: process.env.NODE_ENV === 'development',
                            reloadAll: true
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: { importLoaders: 1 }
                    },
                    'postcss-loader'
                ]
            },

            // Pug
            {
                test: /\.pug$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {}
                    },
                    {
                        loader: 'pug-html-loader',
                        options: {}
                    }
                ]
            }

        ]
    },

    plugins: [

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/pages/index.pug',
            inject: false
        }),

        new MiniCssExtractPlugin({
            filename: "dist.styles.css"
        }),

        // SVG sprite
        // https://github.com/cascornelissen/svg-spritemap-webpack-plugin/blob/HEAD/docs/options.md
        new SVGSpritemapPlugin(
            [ './src/assets/svg-icons/**/*.svg' ],
            {
                sprite: {
                    prefix: 'i-'
                },
                output: {
                    filename: 'dist.icons.svg',
                    svg4everybody: true
                }
            }
        )
    ]

};